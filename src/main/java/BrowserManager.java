import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Collections;

public enum BrowserManager {

    CHROME("chrome") {
        public ChromeOptions getCapabilities() {
            final ChromeOptions options = new ChromeOptions();
            options.addArguments("--allow-silent-push");
            options.addArguments("--disable-extensions");
            options.addArguments("--disable-infobars");
            options.addArguments("--disable-notifications");
            options.setCapability("profile.default_content_setting_values.notifications", 2);
            options.setCapability("chrome.switches", Collections.singletonList("--no-default-browser-check"));
            return options;
        }

        public void webDriverManagerSetup() {
            final WebDriverManager chromedriver = WebDriverManager.chromedriver();
            final String pathToBinary = PropertiesHelper.getInstance().get("local.browser.binary.path.chrome");
            //If you need to use specific chrome version, fill in path to it`s binary in property.file local.browser.binary.path.chrome
            if (StringUtils.isNotEmpty(pathToBinary)) {
                final ChromeOptions options = new ChromeOptions();
                options.setBinary(pathToBinary);
                com.codeborne.selenide.Configuration.browserCapabilities.merge(options);
            }
            chromedriver.setup();
        }
    };

    private final String name;

    BrowserManager(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract Capabilities getCapabilities();

    public abstract void webDriverManagerSetup();

}

