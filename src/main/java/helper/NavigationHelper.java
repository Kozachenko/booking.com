package helper;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebDriver;
import page.HomePage;

import static helper.EndPoints.baseURL;

public class NavigationHelper {

    WebDriver driver;

    public NavigationHelper(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage openHomePage() {
        Selenide.open(baseURL);
        WebDriverRunner.getWebDriver().manage().window().maximize();
        return new page.HomePage().contentLoaded();
    }
}
