import org.apache.commons.lang3.StringUtils;

public class PropertiesHelper {

    private static PropertiesHelper instance;

    public static  synchronized PropertiesHelper getInstance(){
        if(instance == null){
            instance = new PropertiesHelper();
        }
        return instance;
    }

    public String get(final String name) {
        if (StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException("Property key cannot be null or empty.");
        }
        String value = System.getenv(name);
        value = value == null ? System.getProperty(name.replace(".", "_").toUpperCase()) : value;
        value = value == null ? System.getenv(name.replace(".", "_").toUpperCase()) : value;

        return value;
    }
}
