package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.time.Duration;

public class HomePage extends BasePage {

    private static final By ROOT = By.xpath("//span[contains(@data-testid,'herobanner-title1')]");
    private static final By SEARCH_FIELD = By.xpath("//input[@type='search']");
    private static final String CITY_LOCATOR = "//ul[@role='listbox']//span[text() = '%s']";
    private static final String SELECT_DATE_LOCATOR_CURRENT_MONTH = "//div[@class='bui-calendar__content']/div//td[@data-date='%s']";
    private static final String targetField = "//div[@id='xp__guests__inputs-container']//input[@name ='%s']/parent::div/button//following-sibling::span[@class = 'bui-stepper__display']";
    private static final By guestsAndRoomsFieldLocator = By.xpath("//span[@class='xp__guests__count']");
    private static final By searchButtonLocator = By.xpath("//div[contains(@class,'searchbox')]/button[@type='submit']");

    public HomePage contentLoaded() {
        Selenide.$$(ROOT).shouldHave(ROOT_WAIT_CONDITION, Duration.ofMillis(120)).filterBy(Condition.visible);
        return this;
    }

    public HomePage clickOnSearchFieldAndSelectCity(final String city) {
        Selenide.$(SEARCH_FIELD).click();
        Selenide.$(SEARCH_FIELD).sendKeys(city);
        Selenide.$(By.xpath(String.format(CITY_LOCATOR, city))).click();
        return this;
    }

    public HomePage selectDate(final String date) {
        Selenide.$(By.xpath(String.format(SELECT_DATE_LOCATOR_CURRENT_MONTH, date))).click();
        return this;
    }

    public HomePage verifyNumberOfGuestsAndRooms(final String numberOfGuests, final String numberOfRooms) {
        Selenide.$(guestsAndRoomsFieldLocator).click();
        Assert.assertEquals(getFieldValueText("group_adults"), numberOfGuests, "Number of guests is incorrect, please change");
        Assert.assertEquals(getFieldValueText("no_rooms"), numberOfRooms, "Number of rooms is incorrect, please change");
        return this;
    }

    public SearchResultPage clickSearchButton() {
        Selenide.$(searchButtonLocator).click();
        return new SearchResultPage().contentLoaded();
    }

    public String getFieldValueText(final String filedName) {
        return Selenide.$x(String.format(targetField, filedName)).getText();
    }


}