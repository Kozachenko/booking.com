package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;

import java.time.Duration;

public class SearchResultPage extends BasePage {

    private static final By ROOT = By.xpath("//div[@data-component='arp-properties-list']");
    private static final String searchResulTitle = "//div[@data-component='arp-header']//h1[contains(text(),'%s')]";

    public SearchResultPage contentLoaded() {
        Selenide.$$(ROOT).shouldHave(ROOT_WAIT_CONDITION, Duration.ofMillis(120)).filterBy(Condition.visible);
        return this;
    }

    public void verifySearchResultIsCorrectByKeyWord(final String keyWord) {
        Selenide.$(String.format(searchResulTitle, keyWord));
    }
}
