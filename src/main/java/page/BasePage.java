package page;

import com.codeborne.selenide.CollectionCondition;

public class BasePage {

    protected static final CollectionCondition ROOT_WAIT_CONDITION = CollectionCondition.anyMatch("Visible and enabled", e -> e.isDisplayed() && e.isEnabled());
}
