import com.codeborne.selenide.WebDriverRunner;
import com.google.inject.Inject;
import helper.NavigationHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class SelectAccommodationTests extends BaseTest {

    @Inject
    private NavigationHelper navigationHelper;

    @Test
    public void searchAccommodationLvivForTwoPersons() {
        WebDriver driver = WebDriverRunner.getAndCheckWebDriver();
        navigationHelper = new NavigationHelper(driver);
        navigationHelper.openHomePage()
                .clickOnSearchFieldAndSelectCity("Lviv")
                .selectDate("2022-07-10")
                .selectDate("2022-07-17")
                .verifyNumberOfGuestsAndRooms("2", "1")
                .clickSearchButton()
                .verifySearchResultIsCorrectByKeyWord("Львів");
    }
}