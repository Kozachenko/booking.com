import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    @BeforeMethod
    public void configureWebDriver() {
        setUpWebDriverManager();
    }

//    @AfterMethod(alwaysRun = true)
//    public void afterMethod() {
//        WebDriverRunner.closeWebDriver();
//    }

    private void setUpWebDriverManager() {
        final BrowserManager browser = BrowserManager.valueOf("CHROME");
        com.codeborne.selenide.Configuration.browser = browser.getName();
        com.codeborne.selenide.Configuration.browserVersion = PropertiesHelper.getInstance().get("selenoid.browser.version");

        final DesiredCapabilities capabilities = new DesiredCapabilities(browser.getCapabilities());
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", false);
        com.codeborne.selenide.Configuration.browserCapabilities.merge(capabilities);
        browser.webDriverManagerSetup();
    }
}
